var connect= require("connect");
var http= require("http");

function profile(request, response){
    console.log("User requested profile page");
}

function forum(request, response){
    console.log("User requested forum page");
}

var app= connect();

app.use('/profile', profile);
app.use('/forum', forum);

http.createServer(app).listen(8888);
console.log("Server started..");